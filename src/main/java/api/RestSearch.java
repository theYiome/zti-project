package api;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import database.Product;

@Path("/search")
public class RestSearch {
	
	private EntityManagerFactory managerFactory; 
	private EntityManager entityManager;
	private EntityTransaction entityTransaction;

	public RestSearch() {
	   managerFactory = Persistence.createEntityManagerFactory("PU_Postgresql");
	   entityManager = managerFactory.createEntityManager();
	   entityTransaction = entityManager.getTransaction();
	}
    
    @GET
    @Path("{searchQuery}")
    @Produces({MediaType.APPLICATION_JSON})
    public List <Product> get(@PathParam("searchQuery") String searchQuery) {
        System.out.println("/search/" + searchQuery);
        searchQuery = "%"  + searchQuery + "%";
        TypedQuery<Product> query = entityManager.createQuery("SELECT p FROM Product p WHERE UPPER(p.productname) LIKE UPPER(?1)", Product.class);
        List <Product> products = query.setParameter(1, searchQuery).getResultList();
        
        return products;
    }

}