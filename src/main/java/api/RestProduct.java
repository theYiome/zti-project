package api;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import database.Product;

@Path("/product")
public class RestProduct {
	
	private EntityManagerFactory managerFactory; 
	private EntityManager entityManager;
	private EntityTransaction entityTransaction;

	public RestProduct() {
	   managerFactory = Persistence.createEntityManagerFactory("PU_Postgresql");
	   entityManager = managerFactory.createEntityManager();
	   entityTransaction = entityManager.getTransaction();
	}
	
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Product[] get() {
        System.out.println("GET");
		List<Product> people = null;
		try {
			@SuppressWarnings("unchecked")
			List<Product> resultList = (List<Product>) entityManager.createNamedQuery("getAllProducts").getResultList();
			//people = (List<Product>) entityManager.createNamedQuery("findAll").getResultList();
			people = resultList;
			// manager.close();
		} catch (Exception e) {
			System.out.println("Failed !!! " + e.getMessage());
		}
		return people.toArray(new Product[0]);
    }
    
    
    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Product get(@PathParam("id") String id) {
        System.out.println("GET");
        Product entity = (Product) entityManager.find(Product.class, Integer.parseInt(id));
        return entity;
    }
    
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.TEXT_PLAIN})
    public String post(Product product) {
        System.out.println("POST");
		entityTransaction.begin();
		entityManager.persist(product);
		entityManager.flush();
		entityTransaction.commit();
        return "add record" ;
    }

    @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.TEXT_PLAIN})
    public String put(Product product) {
        System.out.println("PUT");
		entityTransaction.begin();
		entityManager.merge(product);
		entityTransaction.commit();
        return "update record" ;
    }

    
    @DELETE
    @Path("{id}")
    @Produces({MediaType.TEXT_PLAIN})
    public String delete(@PathParam("id") String id) {
        System.out.println("DELETE");
		entityTransaction.begin();
        Product entity = (Product) entityManager.find(Product.class, Integer.parseInt(id));
        
        if(entity == null) {
        	entityTransaction.commit();
        	return "record does not exist" ;
        }    	
    	entityManager.remove(entity);			
    	entityManager.flush();
		entityTransaction.commit();
        return "delete record" ;
    }

}