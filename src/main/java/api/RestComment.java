package api;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import database.Comment;
import database.Product;

@Path("/comment")
public class RestComment {
	
	private EntityManagerFactory managerFactory; 
	private EntityManager entityManager; // = managerFactory.createEntityManager();
	private EntityTransaction entityTransaction;

	public RestComment() {
	   managerFactory = Persistence.createEntityManagerFactory("PU_Postgresql");
	   entityManager = managerFactory.createEntityManager();
	   entityTransaction = entityManager.getTransaction();
	}
	
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Comment[] get() {
        System.out.println("GET");
		List<Comment> people = null;
		try {
			@SuppressWarnings("unchecked")
			List<Comment> resultList = (List<Comment>) entityManager.createNamedQuery("getAllComments").getResultList();
			//people = (List<Comment>) entityManager.createNamedQuery("findAll").getResultList();
			people = resultList;
			// manager.close();
		} catch (Exception e) {
			System.out.println("Failed !!! " + e.getMessage());
		}
		return people.toArray(new Comment[0]);
    }
    
    @GET
    @Path("{productid}")
    @Produces({MediaType.APPLICATION_JSON})
    public List <Comment> get(@PathParam("productid") String productid) {
        System.out.println("/commnet/" + productid);
        TypedQuery<Comment> query = entityManager.createQuery("SELECT c FROM Comment c WHERE c.productid = ?1", Comment.class);
        List <Comment> matchedcomments = query.setParameter(1, new BigDecimal(productid)).getResultList();
        
        return matchedcomments;
    }
    
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.TEXT_PLAIN})
    public String post(Comment comment) {
        System.out.println("POST");
        comment.setaccepted(false);
        comment.setcreationdate(new Timestamp(System.currentTimeMillis()));
		entityTransaction.begin();
		entityManager.persist(comment);
		entityManager.flush();
		entityTransaction.commit();
        return "add record" ;
    }

    @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.TEXT_PLAIN})
    public String put(Comment comment) {
        System.out.println("PUT comment");
        comment.setcreationdate(new Timestamp(System.currentTimeMillis()));
		entityTransaction.begin();
		entityManager.merge(comment);
		entityTransaction.commit();
        return "update record" ;
    }

    
    @DELETE
    @Path("{id}")
    @Produces({MediaType.TEXT_PLAIN})
    public String delete(@PathParam("id") String id) {
        System.out.println("DELETE");
		entityTransaction.begin();
        Comment entity = (Comment) entityManager.find(Comment.class, Integer.parseInt(id));
        
        if(entity == null) {
        	entityTransaction.commit();
        	return "record does not exist" ;
        }    	
    	entityManager.remove(entity);			
    	entityManager.flush();
		entityTransaction.commit();
        return "delete record" ;
    }

}