package database;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;


/**
 * The persistent class for the person database table.
 * 
 */
@Entity
@NamedQuery(name="getAllComments", query="SELECT c FROM Comment c ORDER BY c.commentid")
public class Comment implements Serializable {
	private static final long serialVersionUID = 3L;
	private Integer commentid;
	private Integer productid;
	private String content;
	private Timestamp creationdate;
	private Boolean accepted;

	public Comment() {}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Integer getcommentid() {
		return this.commentid;
	}

	public void setcommentid(Integer commentid) {
		this.commentid = commentid;
	}
	
	public Integer getproductid() {
		return this.productid;
	}

	public void setproductid(Integer productid) {
		this.productid = productid;
	}

	public String getcontent() {
		return this.content;
	}

	public void setcontent(String content) {
		this.content = content;
	}
	
	public Timestamp getcreationdate() {
		return this.creationdate;
	}

	public void setcreationdate(Timestamp creationdate) {
		this.creationdate = creationdate;
	}
	
	public Boolean getaccepted() {
		return this.accepted;
	}

	public void setaccepted(Boolean accepted) {
		this.accepted = accepted;
	}
}