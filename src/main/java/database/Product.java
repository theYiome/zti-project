package database;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the person database table.
 * 
 */
@Entity
@NamedQuery(name="getAllProducts", query="SELECT p FROM Product p ORDER BY p.productname")
public class Product implements Serializable {
	private static final long serialVersionUID = 2L;
	private Integer ID;
	private String productname;
	private String shortdescription;
	private String longdescription;
	private Double msrp;

	public Product() {}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Integer getId() {
		return this.ID;
	}

	public void setId(Integer ID) {
		this.ID = ID;
	}

	public String getproductname() {
		return this.productname;
	}

	public void setproductname(String productname) {
		this.productname = productname;
	}
	
	public String getshortdescription() {
		return this.shortdescription;
	}

	public void setshortdescription(String shortdescription) {
		this.shortdescription = shortdescription;
	}

	public String getlongdescription() {
		return this.longdescription;
	}

	public void setlongdescription(String longdescription) {
		this.longdescription = longdescription;
	}
	
	public Double getmsrp() {
		return this.msrp;
	}

	public void setmsrp(Double msrp) {
		this.msrp = msrp;
	}
}