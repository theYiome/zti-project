DROP TABLE IF EXISTS Product;

CREATE TABLE Product (
    id SERIAL PRIMARY KEY,
    productname varchar(128),
    shortdescription varchar(512),
    longdescription varchar(2048),
    msrp FLOAT(32)
);

INSERT INTO Product (productname, shortdescription, longdescription, msrp) VALUES (
    'ACER ACER Nitro N50-610',

    'Komputer ACER ACER Nitro N50-610 to maszyna dla graczy. Całość zamknięta jest w stylowej obudowie z podświetleniem LED.',

    'Etiam ut scelerisque orci. Nullam interdum velit eget sem viverra commodo. In hac habitasse platea dictumst. Nam in sem pellentesque massa convallis accumsan. Nunc ut scelerisque dolor. Sed condimentum risus sit amet commodo mattis. Pellentesque porttitor aliquet nisl vitae aliquam. In nec tempus ligula, vestibulum feugiat mi.',

    3999.99
);

INSERT INTO Product (productname, shortdescription, longdescription, msrp) VALUES (
    'SAMSUNG Galaxy Tab A 8.0',

    'Galaxy Tab A łączy wysoką wydajność i praktyczny design. Dzięki kompaktowemu i smukłemu designowi z łatwością mieści się w dłoni i może towarzyszyć Ci zawsze i wszędzie.',

    'Nam in sem pellentesque massa convallis accumsan. Nunc ut scelerisque dolor. Sed condimentum risus sit amet commodo mattis. Pellentesque porttitor aliquet nisl vitae aliquam. In nec tempus ligula, vestibulum feugiat mi.',

    399.99
);

SELECT * FROM Product;