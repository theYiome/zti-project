
INSERT INTO Product (productname, shortdescription, longdescription, msrp) VALUES (
    'Zipro Bieżnia Pacemaker',

    'Bieżnia Zipro Pacemaker ma masywną konstrukcję która zapewniają jej stabilność w trakcie ćwiczeń oraz duży pas biegowy pozwalający swobodnie prowadzić trening osobom o różnych warunkach fizycznych i technice biegu.',

    'Czerń i złoto odświeżonego designu Zipro idealnie odzwierciedla jej charakter. Klasyka muśnięta nowoczesnym i ekskluzywnym akcentem razem sprawiają, że bieżnia Pacemaker niczym ,,balonik'' na trasie zawodów prowadzi bieg niezależnie od dystansu i pozwala osiągnąć założony cel.',

    3714.99
);

INSERT INTO Product (productname, shortdescription, longdescription, msrp) VALUES (
    'Rowerek Treningowy One S',

    'One S Zipro to nowa odsłona rowerka stacjonarnego przeznaczonego do treningu prowadzonego w warunkach domowych.',

    'Oferuje niczym nieograniczone możliwości dostępu do treningów, dlatego w każdej chwili możesz zażyć dawki ruchu, która pozytywnie wpłynie na kondycję oraz pozwoli wyszczuplać sylwetkę i spalać nadmiar kilogramów. Rower korzysta z magnetycznego systemu oporu. Mechanizm działa cicho, płynnie bez zrywów i charakteryzuje bezawaryjnością.',

    489.99
);

INSERT INTO Product (productname, shortdescription, longdescription, msrp) VALUES (
    'Orbitrek magnetyczny Zipro Neon',

    'Orbitrek magnetyczny Zipro Neon we własnym domu stwarza niczym nieograniczony dostęp do jednej z najbardziej wszechstronnych form treningu.',

    'W trakcie ćwiczeń na orbitreku jednocześnie budujesz kondycję, harmonijnie wzmacniasz mięśnie całego ciała, spalasz kalorie, pozbywasz się zbędnych kilogramów oraz poprawiasz ogólną sprawność swojego organizmu. Regularny trening poprawia witalność i każdorazowo dostarcza dużą dawkę energii na cały dzień.',

    999.99
);

INSERT INTO Product (productname, shortdescription, longdescription, msrp) VALUES (
    'Smartwatch Xlyne Joli XW Pro',

    'Dzięki połączeniu zegarka z aplikacją w telefonie, swobodnie sprawdzisz zebrane przez smartwatch informacje takie jak czas i jakość twojego snu, ilość wykonanych kroków, spalone kalorie czy osiągnięcia w wykonanych aktywnościach.',

    'Jest możliwość ustawienia przypomnień w zegarku tak aby zegarek przypominał Ci o konieczności wypicia wody, o zbyt długim braku ruchu czy umówionych spotkaniach z kalendarza.',

    475.99
);

INSERT INTO Product (productname, shortdescription, longdescription, msrp) VALUES (
    'Nawilżacz powietrza dyfuzor DQ',

    'Ultradźwiękowy nawilżacz powietrza rozprasza zimną wodę za pomocą membrany drgającej z wysoką częstotliwością, przyczyniając się tym samym do utrzymania właściwego poziomu wilgotności',

    'Niewielki rozmiar i atrakcyjny wygląd sprawia, że nawilżacz doskonale wpisuje się w każdą przestrzeń – domową, samochodową oraz biurową. Oświetlenie wygląda absolutnie oszałamiająco, szczególnie przy słabym świetle. Ultradźwiękowa technologia, zapewniająca cichą pracę - pozwoli cieszyć się odpowiednio nawilżonym powietrzem także podczas snu. Posiada funkcję inteligentnego wyłączania, co oznacza, że ​​nie musisz się martwić gdy zapomnisz go wyłączyć.',

    67.99
);

INSERT INTO Product (productname, shortdescription, longdescription, msrp) VALUES (
    'Kosiarka spalinowa RIWALL 51CM 9w1 z napędem 5190',

    'Solidna i oszczędna kosiarka na każdą kieszeń',

    'Silnik spalinowy LONCIN o pojemności 166 cm3 generujący moc 2,9 kW, i wysoki jak na pojemność maksymalny moment obrotowy 9,5 Nm oraz stalowe ostrze o szerokości 51 cm to idealne rozwiązanie dla małych i średnich trawników.',

    1050.99
);

INSERT INTO Product (productname, shortdescription, longdescription, msrp) VALUES (
    'Robot kuchenny planetarny 6L MIKSER 2200W - MOZANO',

    'Najwyższej jakości silnik o dużym momencie obrotowym z metalowym napędem i przekładnią planetarną.',

    'Dzięki w pełni metalowemu napędowi sprzęt zapewnia stabilną, cichą pracę oraz perfekcyjne mieszanie i napowietrzanie mas z wykorzystaniem systemu planetarnego. Mieszadła miksera poruszają się jednocześnie dookoła własnej osi i dookoła misy, co zwiększa efektywność łączenia składników. Wyposażony w dużą stalową misę i 3 rodzaje końcówek dzięki którym przygotujesz wszystkie rodzaje ciasta, kremów i deserów. 6 stopni prędkości, tryb pulsacyjnej pracy oraz podnoszone ramię zapewniają maksymalny komfort pracy. Ponadczasowy design i precyzja wykonania sprawiają, że robot MOZANO to także ozdoba każdej kuchni.',

    499.99
);

INSERT INTO Product (productname, shortdescription, longdescription, msrp) VALUES (
    'Monitor LED Xiaomi Mi 1C 23,8 " IPS FullHD hz',

    'Wysokiej jakości monitor posiadający matrycę IPS, Xiaomi Mi Desktop Monitor 1C 23,8" idealnie sprawdzi się w biurze i jak również w domowym zaciszu!',

    'Panel IPS wyświetla wyraziste i jasne kolory w każdym punkcie wyświetlacza. Zwiększ produktywność pracując na szerokim monitorze i ciesząc się szerokim kątem widzenie 178˚ zapewniającym wierną tonację i odcienie bez blaknących kolorów.',

    614.99
);
