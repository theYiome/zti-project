DROP TABLE IF EXISTS Comment;

CREATE TABLE Comment (
    commentid SERIAL PRIMARY KEY,
    productid INTEGER,
    content varchar(1024),
    creationdate timestamp,
    accepted boolean
);

INSERT INTO Comment (productid, content, creationdate, accepted) VALUES (1, 'Nice, hello!', current_timestamp, TRUE);
INSERT INTO Comment (productid, content, creationdate, accepted) VALUES (1, 'good enough', current_timestamp, TRUE);
INSERT INTO Comment (productid, content, creationdate, accepted) VALUES (1, 'Garbage', current_timestamp, FALSE);
INSERT INTO Comment (productid, content, creationdate, accepted) VALUES (2, 'Meh', current_timestamp, TRUE);
INSERT INTO Comment (productid, content, creationdate, accepted) VALUES (2, 'Hi, i am Mark', current_timestamp, FALSE);

SELECT * FROM Comment;