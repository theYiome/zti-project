const apiServer = "http://localhost:8080/zti-project/";
const searchAPI = apiServer + "api/search/";
const productAPI = apiServer + "api/product/";
const commentAPI = apiServer + "api/comment/";

const app = Vue.createApp({
	data() {
		return {
			adminMode: false,
			showAddNewProduct: false,
			selectedProduct: null
		}
	},

	methods: {
		onProductSelected(id) {
			this.selectedProduct = id;
		},

		onProductDeleted(id) {
			this.selectedProduct = null;
		}
	}
});

app.component("search-page", {
	template: "#search-page-template",

	data() {
		return {
			searchArray: [],
			searchQuery: ""
		}
	},

	methods: {

		search() {
			const config = { mode: "cors" };
			if(this.searchQuery === "") return;
			const path = searchAPI + encodeURIComponent(this.searchQuery);
			fetch(path, config).then(rawData => rawData.json()).then(jsonData => this.searchArray = jsonData);
		}
	}
});

app.component("user-product-page", {
	template: "#user-product-page-template",
	props: ["productid"],

	mounted: function () {
		this.getProduct();
		this.getCommnets();
	},

	data() {
		return {
			productData: null,
			productComments: null,
			
			newCommentContent: "Tu możesz napisać co uważasz o tym produkcie",
			commentSent: false
		}
	},

	methods: {

		getProduct() {
			console.log('getProduct()');
			const config = { mode: "cors" };
			const path = productAPI + encodeURIComponent(this.productid);
			fetch(path, config).then(rawData => rawData.json()).then(jsonData => this.productData = jsonData);
		},

		getCommnets() {
			console.log('getCommnets()');
			const config = { mode: "cors" };
			const path = commentAPI + encodeURIComponent(this.productid);
			fetch(path, config).then(rawData => rawData.json()).then(jsonData => this.productComments = jsonData);
		},

		sendComment() {
			const data = JSON.stringify({
				productid: this.productid,
				content: this.newCommentContent
			});
			console.log(data);
	
			const config = {
				mode: "cors",
				method: "POST",
				headers: {'Content-Type': 'application/json'},
				body: data
			};

			fetch(commentAPI, config).then(() => this.getCommnets());
			this.commentSent = true;
		},

		timestampToString(timestamp) {
			return new Date(timestamp).toLocaleDateString();
		}
	}
});

app.component("admin-product-page", {
	template: "#admin-product-page-template",
	props: ["productid"],

	mounted: function () {
		this.getProduct();
		this.getCommnets();
	},

	data() {
		return {
			productData: null,
			productComments: null,
		}
	},

	methods: {

		getProduct() {
			console.log('getProduct()');
			const config = { mode: "cors" };
			const path = productAPI + encodeURIComponent(this.productid);
			fetch(path, config).then(rawData => rawData.json()).then(jsonData => this.productData = jsonData);
		},

		getCommnets() {
			console.log('getCommnets()');
			const config = { mode: "cors" };
			const path = commentAPI + encodeURIComponent(this.productid);
			fetch(path, config).then(rawData => rawData.json()).then(jsonData => this.productComments = jsonData);
		},

		updateProduct() {
			const data = JSON.stringify(this.productData);

			console.log(data);
	
			if(this.productData.productname == "") return;

			const config = {
				mode: "cors",
				method: "PUT",
				headers: {'Content-Type': 'application/json'},
				body: data
			};

			fetch(productAPI, config).then(response => console.log(response)).then(() => this.getProduct());
		},

		acceptComment(index) {
			const comment = this.productComments[index];
			comment.accepted = true;

			const data = JSON.stringify(comment);

			console.log(data);

			const config = {
				mode: "cors",
				method: "PUT",
				headers: {'Content-Type': 'application/json'},
				body: data
			};

			fetch(commentAPI, config).then(response => console.log(response)).then(() => this.getCommnets());
		},

		deleteProduct() {
			const config = {
				mode: "cors",
				method: "DELETE"
			};

			const path = productAPI + encodeURIComponent(this.productid);

			fetch(path, config).then(response => console.log(response));
			this.$emit("productDeleted", this.productid)
		},


		deleteComment(index) {
			const config = {
				mode: "cors",
				method: "DELETE"
			};
			const id = this.productComments[index].commentid;
			const path = commentAPI + encodeURIComponent(id);

			fetch(path, config).then(response => console.log(response)).then(() => this.getCommnets());
		},

		timestampToString(timestamp) {
			return new Date(timestamp).toLocaleDateString();
		}
	}
});

app.component("add-new-product", {
	template: "#add-new-product-template",

	data() {
		return {
			newProductData: {
				productname: "",
				shortdescription: "",
				longdescription: "",
				msrp: null
			},
			newProductAdded: false
		}
	},

	methods: {

		addNewProduct() {
			const data = JSON.stringify(this.newProductData);

			console.log(data);
	
			if(this.newProductData.productname == "") return;

			const config = {
				mode: "cors",
				method: "POST",
				headers: {'Content-Type': 'application/json'},
				body: data
			};

			fetch(productAPI, config).then(response => console.log(response));
			this.newProductAdded = true;
		},

		resert() {
			this.newProductAdded = false;

			this.newProductData = {
				productname: "",
				shortdescription: "",
				longdescription: "",
				msrp: null
			};
		}
	}
});

app.mount("#app");

