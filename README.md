## Opis
Klient wykonany z vue.js, wykorzystano między innymi:
- data
- methods
- template (text/x-template)
- v-bind (:property)
- v-on (@event)
- v-if
- v-else
- v-for
- v-model
- lifecycle hooks (mounted)
- components
- props

## Linki
### Wersja demonstracjna
https://vue-zti-project.eu-gb.mybluemix.net/

### Repozytroium
https://gitlab.com/theYiome/zti-project

### Plik .war do pobrania (link tymczasowy)
https://files.fm/u/cthyayhs4
